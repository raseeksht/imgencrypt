import express from 'express';
import { fileURLToPath } from 'url';
import path from 'path';

import mongoose from 'mongoose';

const encryptSchema = mongoose.Schema({
    encryptedHex: {
        type: String,
    },
    name: {
        type: String
    }
})

const model = mongoose.model("encryptedpic", encryptSchema);

mongoose.connect(process.env.MONGO_URI || "mongodb://127.0.0.1:27017/encryptedpicdb").then(data => {
    console.log("mongodb connected")
}).catch(err => {
    console.log(err)
})


const app = express();
app.use(express.json({ limit: '10mb' }));


const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename)

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/img.html");
})

app.post("/add", async (req, res) => {
    const { encryptedHex, name } = req.body;
    console.log(encryptedHex, name)
    const enc = await model.create({ encryptedHex, name })
    res.json({ message: "uplaod done", name: enc.name })
})

app.get("/getnames", async (req, res) => {
    const names = await model.find({}, { name: 1 });
    res.json(names)
})

app.get("/getimage", async (req, res) => {
    const _id = req.query._id;
    const encryptedHex = await model.findOne({ _id })
    res.json(encryptedHex)
})



app.listen(3000);